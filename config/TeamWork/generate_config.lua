local script_name = "TeamWork"

local config = {
   general = {
      on = true,
   },
   team = {
      "Siroja_Savushkin",
      "Sveta_Asadchik"
   },
   commands = {
      callcar = true,
      lock = true,
      fixcar = true,
      hl = false,
      hr = false,
   }
}

inicfg.save(config, script_name .. "\\default.ini")