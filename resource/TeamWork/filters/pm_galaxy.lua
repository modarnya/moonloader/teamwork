local REGEX = ">> �� �� (%S+)%(%d+%):%s.+"
local MSG_COLOR = 0xFFFF22AA
local NICKNAME_IDX = 1

local function is_nick_suitable(tbl, value)
   for _, element in pairs(tbl) do
      local _value = string.lower(value)
      local _element = string.lower(element)
      if _value == _element then
         return true
      end
   end
   return false
end

return function(color, text, team)
   if bit.tohex(color) == bit.tohex(MSG_COLOR) then
      local recieved_nick = select(NICKNAME_IDX, text:match(REGEX))
      if is_nick_suitable(team, recieved_nick) then
         return true
      end
   end
   return false
end