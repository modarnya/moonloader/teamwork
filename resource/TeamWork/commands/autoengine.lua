local response = "/engine"

local cmd = {
   info = "automatic /engine",
   filter  = "no_filter",
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   func    = nil
}
---
local trigger_text = {
   "^����� ������� ���������, ������ %{33AA33%}%/engine%{FFFFFF%} ��� ����� ������� %{33AA33%}NUM 4%{FFFFFF%}.$",
   "^��������� ������.$",
   "^��������� ������ ��������. ������ %/mechanic ��� ������ ��������, ���� %/taxi ��� ������ �����.$",
   "^����� ������� ��������� �� �����, ������ %/lock.$",
   "^�������. ����������� � �����, ���������� �� �����.$",
}

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value:find(element) then
         return true
      end
   end
   return false
end

cmd.func = lua_thread.create_suspended(
function(_, text)
   if isCharInAnyCar(PLAYER_PED) and is_in_array(trigger_text, text) then
      wait(0)
      sampSendChat(response)
   end
end)

return cmd