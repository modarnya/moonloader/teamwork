local PM_REGEX = ">> �� �� (%S+)%(%d+%):%s.+"

local cmd = {
   info = '/fmute <id/nick> <time> "<reason>" command trigger (team only)',
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   filter  = "pm_galaxy",
   func    = nil
}

cmd.func = lua_thread.create_suspended(
function(_, text)
   wait(0)
   local res_cmd = nil
   local player, time, reason = text:match('/fmute (%S+)%s(%d+)%s(%b"")')
   if player and time and reason then
      reason = string.gsub(reason, '"', "")
      local nick = text:match(PM_REGEX)
      res_cmd = string.format("/fmute %s %d %s (c) %s", player, time, reason, nick)
   else
      player, time = text:match("/fmute (%S+)%s(%d+)")
      if not player or not time then return end
      res_cmd = string.format("/fmute %s %d", player, time)
   end
   print(res_cmd)
   sampSendChat(res_cmd)
end)

return cmd